package chypher;

import java.util.Arrays;

/**
 *
 * @author andrea
 */
public class Chypher {

    static char[] coder = new char[26];
    static int i = 0;
    static int pos = 0;
    static int num = 0;
    static int pos2 = 0;
    static String parola = "";

    public static void main(String[] args) {
        int[] numeri = new int[0];
        System.out.println(Arrays.toString(chypher("zappa", numeri)));
        System.out.println(dechypher());
    }

    public static int[] chypher(String s, int[] codice) {
        while (num < coder.length) {
            int number = 97 + num;
            char character = (char) number;
            coder[num] += character;
            num++;
        }
        codice = new int[s.length()];
        while (i < s.length()) {
            char conf = s.charAt(i);
            if (conf == (coder[pos])) {
                codice[i] += pos + 5 * Math.pow(pos, 1 / 3);
                i++;
                pos = 0;
            } else {
                pos++;
            }
        }
        parola = s;
        return codice;
    }

    public static String dechypher() {
        return parola;
    }
}
