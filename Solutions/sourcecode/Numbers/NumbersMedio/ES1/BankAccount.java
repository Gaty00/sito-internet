package banckaccounttest;

import java.util.ArrayList;

public class BankAccount {

    private double deposit = 0;
    private int n = 0;
    ArrayList<String> count = new ArrayList();

    public double withdraw(double acount) {
        if (deposit - acount >= 0) {
            deposit -= acount;
            count.add(n, "withdraw: "+acount);
            n++;
        }
        return deposit;
    }

    public double deposit(double acount) {
        deposit += acount;
        count.add(this.n, "deposit: "+ acount);
        n++;
        return deposit;
    }

    public double getDeposit() {
        count.add(n, "getDeposit: "+deposit);
        n++;
        return deposit;
    }
    public ArrayList<String> getCount(){
    return count;
    
    }
    public void resetCount(){
        count.clear();
        deposit = 0;
        
    }
}
