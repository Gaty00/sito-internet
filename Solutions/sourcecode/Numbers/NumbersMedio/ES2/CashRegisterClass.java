/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cashregister;

import java.util.ArrayList;

/**
 *
 * @author andrea
 */
public class CashRegisterClass {

    double costo;
    double resto;
    ArrayList<Double> prodotti = new ArrayList<>();
    ArrayList<String> scontrino = new ArrayList<>();

    double cost(double prezzo, double saldo, int numero) {
        
        costo = (prezzo - (prezzo*saldo / 100)) * numero;
        return costo;
    }

    double payment(double money) {
        if (money >= this.costo) {
            money -= this.costo;
            this.scontrino.add("pagamento: " + this.costo);
        }

        return money;
    }
    ArrayList<Double> scontrino(ArrayList<Double> a) {
        return a;
    }
}
