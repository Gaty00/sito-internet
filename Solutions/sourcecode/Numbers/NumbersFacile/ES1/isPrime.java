package esercizio1;

/**
 *
 * @author andrea
 */
public class Esercizio7 {

    public static void main(String[] args) {
        boolean ris = isPrime(7);
        System.out.println(ris);
    }

    public static boolean isPrime(int n) {
        int x = 2;
        boolean risult = true;
        while (x < n) {
            if (n % x == 0) {
                risult = false;
                x = n;

            }
            x++;

        }

        return risult;

    }
}
