package es10;

/**
 *
 * @author Andrea
 */
class Divisione extends espressione {

    Divisione(int n1, int n2) {
        super(n1 / n2);

    }

    @Override
    public double valuta() {
        return super.getRisultato();
    }
}
