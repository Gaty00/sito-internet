package es10;

/**
 *
 * @author Andrea
 */
public class Sottrazione extends espressione {
    
    Sottrazione(int n1, int n2){
        super(n1-n2);
    
    }

    @Override
    public double valuta() {
        return super.getRisultato();
    }
}
