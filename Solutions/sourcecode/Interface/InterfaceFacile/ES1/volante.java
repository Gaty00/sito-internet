/**
 *
 * @author Andrea
 */
public class volante implements fabbrica{
    Object volante;
    @Override
    public String iniziaAProdurre(String volante) {
        this.volante=volante;
        System.out.println("iniziata produzione componente");
        System.out.println(timer());
        return volante;
    }

    @Override
    public Object prodotto() {
        return volante;
    }

    @Override
    public double timer() {
        return Math.random();
    }

}
