/**
 *
 * @author Andrea
 */
public interface fabbrica {
    Object iniziaAProdurre(String auto);
    Object prodotto();
    double timer();
}
