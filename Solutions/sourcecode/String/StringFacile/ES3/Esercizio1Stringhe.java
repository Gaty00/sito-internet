package esercizio.pkg1.stringhe;

/*
Acquisire in ingresso una frase, eliminare gli spazi multipli (lasciandone solo uno) e alla fine visualizzarla.

Esempio:      “la    casa    del     vicino”   ->>    “la casa del vicino”

*/
import java.util.Scanner;

public class Esercizio1Stringhe {

    private static Object input;

    public static void main(String[] args) {
        String risultato = "";
        Scanner Input = new Scanner(System.in);
        System.out.print("inserisci la frase: ");
        String frase = Input.nextLine();
        int length = frase.length();
        for (int i = 0; i < length; i++) {
            if (frase.charAt(i) == ' ' && frase.charAt(i + 1) == ' ') {
            } else {
                risultato = risultato + frase.charAt(i);
            }
        }
        System.out.println("frase: " + risultato);
    }

}
