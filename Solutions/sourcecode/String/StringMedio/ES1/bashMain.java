﻿package bash;

import java.util.Arrays;
import java.util.Scanner;

public class bashMain {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        BashClass b = new BashClass();
        System.out.println("scrivi 3 cose spaziate da un invio");
        String elem0 = input.nextLine().toLowerCase();
        String elem1 = input.nextLine().toLowerCase();
        String elem2 = input.nextLine().toLowerCase();
        String[] array = new String[3];
        array[0] = elem0;
        array[1] = elem1;
        array[2] = elem2;
        System.out.println("scrivi cerca se vuoi cercare gli elementi");
        System.out.println("scrivi mostra se vuoi vedere gli elementi");
        System.out.println("scrivi cat se vuoi sovrascrivere gli elementi");
        String cond = input.nextLine().toLowerCase();
        if (cond.equals("cerca")) {
            System.out.println("Scrivi ciò che vuoi trovare");
            String cerca = input.nextLine();
            System.out.println(Arrays.toString(b.basicEgrep(array, cerca)));
        }
        if (cond.equals("mostra")) {
            System.out.println(Arrays.toString(b.ls(array)));
        }
        if (cond.equals("cat")) {
            System.out.println("Scrivi il nome del file che vuoi modificare");
            String cerca = input.nextLine();
            System.out.println("Scrivi ciò che vuoi");
            String modifica = input.nextLine();
            System.out.println(Arrays.toString(b.cat(cerca, array, modifica)));
        }

    }

}
